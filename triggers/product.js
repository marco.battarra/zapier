const sample = require('../samples/sample_product');

const triggerProduct = (z, bundle) => {
    return z.request({
        url: 'http://api.cassanova.com/apikey/token',
        headers: {
            'X-Requested-With': '*',
            'Content-Type': 'application/json'
        },
        body: {
          'apiKey': `${bundle.authData.apiKey}`
        },
        params: {},
        method: 'POST'
    })
    .then(response => {
      const answ = JSON.parse(response.content);
      const token = answ.access_token;
      const par = {
        method: 'GET',
        url: 'http://api.cassanova.com/products',
        body:  {},
        headers: {
          'X-Version': '1.0.0',
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`
        },
        params: {
          start: 0,//bundle.inputData.start,
          limit: 100//bundle.inputData.limit
        }
      };
      return z.request(par)
        .then(response2 => {
          return(JSON.parse(response2.content).products)
	});
    });
};

module.exports = {

  key: 'product',

  noun: 'product',

  display: {
    label: 'Get Products',
    description: 'Triggers on a new product.'
  },

  operation: {
    inputFields: [
      {key: 'start', label:'start', required: true, choices: {0:'0',10:'10',20:'20'}},
      {key:'limit', required: true, label: 'limit', choices: {50:'50',100:'100'}}
    ],
    perform: triggerProduct,
    sample: sample
  }

};
