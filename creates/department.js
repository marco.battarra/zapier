const sample = require('../samples/sample_product');

const createDepartment = (z, bundle) => {
    return z.request({
        url: 'http://api.cassanova.com/apikey/token',
        headers: {
            'X-Requested-With': '*',
            'Content-Type': 'application/json'
        },
        body: {
          'apiKey': `${bundle.authData.apiKey}`
        },
        params: {},
        method: 'POST'
    })
    .then(response => {
      const answ = JSON.parse(response.content);
      const token = answ.access_token;

      var externalId = "";
      if (`${bundle.inputData.externalId}` == 'undefined') { externalId = null }
      else { externalId = `${bundle.inputData.externalId}` }

      var idSalesPoint = "";
      if (`${bundle.inputData.idSalesPoint}` == 'undefined') { idSalesPoint = null }
      else { idSalesPoint = `${bundle.inputData.idSalesPoint}` }

      var amountLimit = "";
      if (`${bundle.inputData.amountLimit}` == 'undefined') { amountLimit = null }
      else { amountLimit = `${bundle.inputData.amountLimit}` }

      const par = {
        method: 'POST',
        url: 'https://api.cassanova.com/departments',
        body: {
          'description': `${bundle.inputData.description}`,
          'descriptionLabel': `${bundle.inputData.descriptionLabel}`,
          'descriptionReceipt': `${bundle.inputData.descriptionReceipt}`,
          'idTax': `${bundle.inputData.idTax}`,
          'color': `${bundle.inputData.color}`,
          'amountLimit': amountLimit,
          'externalId': externalId,
          'idSalesPoint': idSalesPoint
        },
        headers: {
          'X-Version': '1.0.0',
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`,
          'X-Requested-With': '*'
        },
        params: {}
      };
      return z.request(par)
        .then(response2 => {
          return(JSON.parse(response2.content))
        });
    });
};

module.exports = {
  key: 'department',
  noun: 'Department',

  display: {
    label: 'Create Department',
    description: 'Creates a department.'
  },

  operation: {
    inputFields: [
      {key: 'description', label:'description', required: true},
      {key: 'descriptionLabel', label:'descriptionLabel', required: true},
      {key: 'descriptionReceipt', label:'descriptionReceipt', required: true},
      {key: 'idTax', label:'idTax', required: true},
      {key: 'color', label:'color', required: true},
      {key: 'amountLimit', label:'amountLimit', required: false},
      {key: 'externalId', label:'externalId', required: false},
      {key: 'idSalesPoint', label:'idSalesPoint', required: false}
    ],
    perform: createDepartment,
    sample: sample
  }
};
