const sample = require('../samples/sample_product');

const createProduct = (z, bundle) => {
    return z.request({
        url: 'http://api.cassanova.com/apikey/token',
        headers: {
            'X-Requested-With': '*',
            'Content-Type': 'application/json'
        },
        body: {
          'apiKey': `${bundle.authData.apiKey}`
        },
        params: {},
        method: 'POST'
    })
    .then(response => {
      const answ = JSON.parse(response.content);
      const token = answ.access_token;

      var externalId = "";
      if (`${bundle.inputData.externalId}` == 'undefined') { externalId = null }
      else { externalId = `${bundle.inputData.externalId}` }

      var internalId = "";
      if (`${bundle.inputData.internalId}` == 'undefined') { internalId = null }
      else { internalId = `${bundle.inputData.internalId}` }

      var idSalesPoint = "";
      if (`${bundle.inputData.idSalesPoint}` == 'undefined') { idSalesPoint = null }
      else { idSalesPoint = `${bundle.inputData.idSalesPoint}` }

      var defaultTare = "";
      if (`${bundle.inputData.defaultTare}` == 'undefined') { defaultTare = [] }
      else { defaultTare = `${bundle.inputData.defaultTare}` }

      var color = "";
      if (`${bundle.inputData.color}` == 'undefined') { color = [] }
      else { color = `${bundle.inputData.color }` }

      var costs = "";
      if (`${bundle.inputData.costs}` == 'undefined') { costs = [] }
      else { costs = `${bundle.inputData.costs }` }

      var descriptionReceipt = "";
      if (`${bundle.inputData.descriptionReceipt}` == 'undefined') { descriptionReceipt = [] }
      else { descriptionReceipt = `${bundle.inputData.descriptionReceipt }` }

      var barcodes = "";
      if (`${bundle.inputData.barcodes}` == 'undefined') { barcodes = [] }
      else { barcodes = `${bundle.inputData.barcodes }` }

      var alternatives = "";
      if (`${bundle.inputData.alternatives}` == 'undefined') { alternatives = [] }
      else { alternatives = `${bundle.inputData.alternatives }` }

      var relateds = "";
      if (`${bundle.inputData.relateds}` == 'undefined') { relateds = [] }
      else { relateds = `${bundle.inputData.relateds }` }

      const par = {
        method: 'POST',
        url: 'https://api.cassanova.com/products',
        body: {
          'description': `${bundle.inputData.description}`,
          'descriptionLabel': `${bundle.inputData.descriptionLabel}`,
          'descriptionExtended': `${bundle.inputData.descriptionExtended}`,
          'idDepartment': `${bundle.inputData.idDepartment}`,
          'idCategory': `${bundle.inputData.idCategory}`,
          'icon': `${bundle.inputData.icon}`,
          'soldByWeight': JSON.parse(`${bundle.inputData.soldByWeight}`),
          'multivariant': JSON.parse(`${bundle.inputData.multivariant}`),
          'defaultTare': defaultTare,
          'multivariant': JSON.parse(`${bundle.inputData.multivariant}`),
          'color': color,
          'enableForSale': JSON.parse(`${bundle.inputData.enableForSale}`),
          'enableForRisto': JSON.parse(`${bundle.inputData.enableForRisto}`),
          'enableForECommerce': JSON.parse(`${bundle.inputData.enableForECommerce}`),
          'enableForMobileCommerce': JSON.parse(`${bundle.inputData.enableForMobileCommerce}`),
          'enableForKiosk': JSON.parse(`${bundle.inputData.enableForKiosk}`),
          'enableForSelfOrderMenu': JSON.parse(`${bundle.inputData.enableForSelfOrderMenu}`),
          'tags': JSON.parse(`${bundle.inputData.tags}`),
          'prices': JSON.parse(`${bundle.inputData.prices}`),
          'costs': costs,
          'externalId': externalId,
          'idSalesPoint': idSalesPoint,
          'descriptionReceipt': descriptionReceipt,
          'internalId': internalId,
          'barcodes': barcodes,
          'alternatives': alternatives,
          'relateds': relateds,
          'attributes': JSON.parse(`${bundle.inputData.attributes}`),
          'modifiers': JSON.parse(`${bundle.inputData.modifiers}`),
          'images': JSON.parse(`${bundle.inputData.images}`)
        },
        headers: {
          'X-Version': '1.0.0',
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`,
          'X-Requested-With': '*'
        },
        params: {}
      };
      return z.request(par)
        .then(response2 => {
          return(JSON.parse(response2.content))
        });
    });
};

module.exports = {
  key: 'product',
  noun: 'Product',

  display: {
    label: 'Create Product',
    description: 'Creates a product.'
  },

  operation: {
    inputFields: [
      {key: 'description', label:'description', required: true},
      {key: 'descriptionLabel', label:'descriptionLabel', required: true},
      {key: 'descriptionExtended', label:'descriptionExtended', required: true},
      {key: 'idDepartment', label:'idDepartment', required: true},
      {key: 'idCategory', label:'idCategory', required: true},
      {key: 'icon', label:'icon', required: true},
      {key: 'soldByWeight', label:'soldByWeight', required: true},
      {key: 'defaultTare', label:'defaultTare', required: false},
      {key: 'multivariant', label:'multivariant', required: true},
      {key: 'color', label:'color', required: false},
      {key: 'enableForSale', label:'enableForSale', required: true},
      {key: 'enableForRisto', label:'enableForRisto', required: true},
      {key: 'enableForECommerce', label:'enableForECommerce', required: true},
      {key: 'enableForMobileCommerce', label:'enableForMobileCommerce', required: true},
      {key: 'enableForKiosk', label:'enableForKiosk', required: true},
      {key: 'enableForSelfOrderMenu', label:'enableForSelfOrderMenu', required: true},
      {key: 'tags', label:'tags', required: true},
      {key: 'costs', label:'costs', required: false},
      {key: 'externalId', label:'externalId', required: false},
      {key: 'idSalesPoint', label:'idSalesPoint', required: false},
      {key: 'descriptionReceipt', label:'descriptionReceipt', required: false},
      {key: 'internalId', label:'internalId', required: false},
      {key: 'barcodes', label:'barcodes', required: false},
      {key: 'alternatives', label:'alternatives', required: false},
      {key: 'relateds', label:'relateds', required: false},
      {key: 'prices', label:'prices', required: true},
      {key: 'attributes', label:'attributes', required: true},
      {key: 'modifiers', label:'modifiers', required: true},
      {key: 'images', label:'images', required: true}
    ],
    perform: createProduct,
    sample: sample
  }
};
