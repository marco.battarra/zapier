const sample = require('../samples/sample_product');

const createSalesMode = (z, bundle) => {
    return z.request({
        url: 'http://api.cassanova.com/apikey/token',
        headers: {
            'X-Requested-With': '*',
            'Content-Type': 'application/json'
        },
        body: {
          'apiKey': `${bundle.authData.apiKey}`
        },
        params: {},
        method: 'POST'
    })
    .then(response => {
      const answ = JSON.parse(response.content);
      const token = answ.access_token;

      var idSalesPoint = "";
      if (`${bundle.inputData.idSalesPoint}` == 'undefined') { idSalesPoint = null }
      else { idSalesPoint = `${bundle.inputData.idSalesPoint}` }

      const par = {
        method: 'POST',
        url: 'https://api.cassanova.com/salesmodes',
        body: {
          'description': `${bundle.inputData.description}`,
          'idSalesPoint': idSalesPoint
        },
        headers: {
          'X-Version': '1.0.0',
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`,
          'X-Requested-With': '*'
        },
        params: {}
      };
      return z.request(par)
        .then(response2 => {
          return(JSON.parse(response2.content))
        });
    });
};

module.exports = {
  key: 'salesMode',
  noun: 'SalesMode',

  display: {
    label: 'Create Sales Mode',
    description: 'Creates a sales Mmde.'
  },

  operation: {
    inputFields: [
      {key: 'description', label:'description', required: true},
      {key: 'idSalesPoint', label:'idSalesPoint', required: false},
    ],
    perform: createSalesMode,
    sample: sample
  }
};
