const sample = require('../samples/sample_product');

const createSupplier = (z, bundle) => {
    return z.request({
        url: 'http://api.cassanova.com/apikey/token',
        headers: {
            'X-Requested-With': '*',
            'Content-Type': 'application/json'
        },
        body: {
          'apiKey': `${bundle.authData.apiKey}`
        },
        params: {},
        method: 'POST'
    })
    .then(response => {
      const answ = JSON.parse(response.content);
      const token = answ.access_token;

      var taxCode = "";
      if (`${bundle.inputData.taxCode}` == 'undefined') { taxCode = null }
      else { taxCode = `${bundle.inputData.taxCode}` }

      var street = "";
      if (`${bundle.inputData.street}` == 'undefined') { street = null }
      else { street = `${bundle.inputData.street}` }

      var city = "";
      if (`${bundle.inputData.city}` == 'undefined') { city = null }
      else { city = `${bundle.inputData.city}` }

      var zipcode = "";
      if (`${bundle.inputData.zipcode}` == 'undefined') { zipcode = null }
      else { zipcode = `${bundle.inputData.zipcode}` }

      var district = "";
      if (`${bundle.inputData.district}` == 'undefined') { district = null }
      else { district = `${bundle.inputData.district}` }

      var country = "";
      if (`${bundle.inputData.country}` == 'undefined') { country = null }
      else { country = `${bundle.inputData.country}` }

      var phoneNumber = "";
      if (`${bundle.inputData.phoneNumber}` == 'undefined') { phoneNumber = null }
      else { phoneNumber = `${bundle.inputData.phoneNumber}` }

      var email = "";
      if (`${bundle.inputData.email}` == 'undefined') { email = null }
      else { email = `${bundle.inputData.email}` }

      var note = "";
      if (`${bundle.inputData.note}` == 'undefined') { note = null }
      else { note = `${bundle.inputData.note}` }

      var note = "";
      if (`${bundle.inputData.note}` == 'undefined') { note = null }
      else { note = `${bundle.inputData.note}` }

      var externalId = "";
      if (`${bundle.inputData.externalId}` == 'undefined') { externalId = null }
      else { externalId = `${bundle.inputData.externalId}` }

      var idSalesPoint = "";
      if (`${bundle.inputData.idSalesPoint}` == 'undefined') { idSalesPoint = null }
      else { idSalesPoint = `${bundle.inputData.idSalesPoint}` }

      const par = {
        method: 'POST',
        url: 'https://api.cassanova.com/suppliers',
        body: {
          'name': `${bundle.inputData.name}`,
          'vatNumber': `${bundle.inputData.vatNumber}`,
          'taxCode': taxCode,
          'street': street,
          'city': city,
          'zipcode': zipcode,
          'district': district,
          'country': country,
          'phoneNumber': phoneNumber,
          'email': email,
          'note': note,
          'externalId': externalId,
          'idSalesPoint': idSalesPoint,
        },
        headers: {
          'X-Version': '1.0.0',
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`,
          'X-Requested-With': '*'
        },
        params: {}
      };
      return z.request(par)
        .then(response2 => {
          return(JSON.parse(response2.content))
        });
    });
};

module.exports = {
  key: 'supplier',
  noun: 'Supplier',

  display: {
    label: 'Create Supplier',
    description: 'Creates a supplier.'
  },

  operation: {
    inputFields: [
      {key: 'name', label:'name', required: true},
      {key: 'vatNumber', label:'vatNumber', required: true},
      {key: 'taxCode', label:'taxCode', required: false},
      {key: 'street', label:'street', required: false},
      {key: 'city', label:'city', required: false},
      {key: 'zipcode', label:'zipcode', required: false},
      {key: 'district', label:'district', required: false},
      {key: 'country', label:'country', required: false},
      {key: 'phoneNumber', label:'phoneNumber', required: false},
      {key: 'email', label:'email', required: false},
      {key: 'note', label:'note', required: false},
      {key: 'externalId', label:'externalId', required: false},
      {key: 'idSalesPoint', label:'idSalesPoint', required: false},
    ],
    perform: createSupplier,
    sample: sample
  }
};
