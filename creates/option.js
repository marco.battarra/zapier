const sample = require('../samples/sample_product');

const createOption = (z, bundle) => {
    return z.request({
        url: 'http://api.cassanova.com/apikey/token',
        headers: {
            'X-Requested-With': '*',
            'Content-Type': 'application/json'
        },
        body: {
          'apiKey': `${bundle.authData.apiKey}`
        },
        params: {},
        method: 'POST'
    })
    .then(response => {
      const answ = JSON.parse(response.content);
      const token = answ.access_token;

      var externalId = "";
      if (`${bundle.inputData.externalId}` == 'undefined') { externalId = null }
      else { externalId = `${bundle.inputData.externalId}` }

      var idSalesPoint = "";
      if (`${bundle.inputData.idSalesPoint}` == 'undefined') { idSalesPoint = null }
      else { idSalesPoint = `${bundle.inputData.idSalesPoint}` }

      var values = "";
      if (`${bundle.inputData.values}` == 'undefined') { values = [] }
      else { values = `${bundle.inputData.values}` }

      const par = {
        method: 'POST',
        url: 'https://api.cassanova.com/options',
        body: {
          'description': `${bundle.inputData.description}`,
          'externalId': externalId,
          'idSalesPoint': idSalesPoint,
          'values': values
        },
        headers: {
          'X-Version': '1.0.0',
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`,
          'X-Requested-With': '*'
        },
        params: {}
      };
      return z.request(par)
        .then(response2 => {
          return(JSON.parse(response2.content))
        });
    });
};

module.exports = {
  key: 'option',
  noun: 'Option',

  display: {
    label: 'Create Option',
    description: 'Creates a option.'
  },

  operation: {
    inputFields: [
      {key: 'description', label:'description', required: true},
      {key: 'values', label: 'values', required: false},
      {key: 'externalId', label:'externalId', required: false},
      {key: 'idSalesPoint', label:'idSalesPoint', required: false},
    ],
    perform: createOption,
    sample: sample
  }
};
