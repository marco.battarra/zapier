const sample = require('../samples/sample_product');

const createCustomer = (z, bundle) => {
    return z.request({
        url: 'http://api.cassanova.com/apikey/token',
        headers: {
            'X-Requested-With': '*',
            'Content-Type': 'application/json'
        },
        body: {
          'apiKey': `${bundle.authData.apiKey}`
        },
        params: {},
        method: 'POST'
    })
    .then(response => {
      const answ = JSON.parse(response.content);
      const token = answ.access_token;

      var idOrganization = "";
      if (`${bundle.inputData.idOrganization}` == 'undefined') { idOrganization = null }
      else { idOrganization = `${bundle.inputData.idOrganization}` }

      var country = "";
      if (`${bundle.inputData.country}` == 'undefined') { country = null }
      else { country = `${bundle.inputData.country}` }

      var vatNumber = "";
      if (`${bundle.inputData.vatNumber}` == 'undefined') { vatNumber = null }
      else { vatNumber = `${bundle.inputData.vatNumber}` }

      var fiscalCode = "";
      if (`${bundle.inputData.fiscalCode}` == 'undefined') { fiscalCode = null }
      else { fiscalCode = `${bundle.inputData.fiscalCode}` }

      var phoneNumber = "";
      if (`${bundle.inputData.phoneNumber}` == 'undefined') { phoneNumber = null }
      else { phoneNumber = `${bundle.inputData.phoneNumber}` }

      var email = "";
      if (`${bundle.inputData.email}` == 'undefined') { email = null }
      else { email = `${bundle.inputData.email}` }

      var note = "";
      if (`${bundle.inputData.note}` == 'undefined') { note = null }
      else { note = `${bundle.inputData.note}` }

      var name = "";
      if (`${bundle.inputData.name}` == 'undefined') { name = null }
      else { name = `${bundle.inputData.name}` }

      var externalId = "";
      if (`${bundle.inputData.externalId}` == 'undefined') { externalId = null }
      else { externalId = `${bundle.inputData.externalId}` }

      var idSalesPoint = "";
      if (`${bundle.inputData.idSalesPoint}` == 'undefined') { idSalesPoint = null }
      else { idSalesPoint = `${bundle.inputData.idSalesPoint}` }

      const par = {
        method: 'POST',
        url: 'https://api.cassanova.com/customers',
        body: {
          'idOrganization': idOrganization,
          'dateOfBirth': `${bundle.inputData.dateOfBirth}`,
          'gender': `${bundle.inputData.gender}`,
          'address': `${bundle.inputData.address}`,
          'city': `${bundle.inputData.city}`,
          'zipcode': `${bundle.inputData.zipcode}`,
          'district': `${bundle.inputData.district}`,
          'country': country,
          'vatNumber': vatNumber,
          'fiscalCode': fiscalCode,
          'phoneNumber': phoneNumber,
          'email': email,
          'note': note,
          'name': name,
          'externalId': externalId,
          'idSalesPoint': idSalesPoint
        },
        headers: {
          'X-Version': '1.0.0',
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`,
          'X-Requested-With': '*'
        },
        params: {}
      };
      console.log(par);
      return z.request(par)
        .then(response2 => {
          return(JSON.parse(response2.content))
        });
    });
};

module.exports = {
  key: 'customer',
  noun: 'Customer',

  display: {
    label: 'Create Customer',
    description: 'Creates a customer.'
  },

  operation: {
    inputFields: [
      {key: 'idOrganization', label: 'idOrganization', required: false},
      {key: 'name', label: 'name', required: true},
      {key: 'dateOfBirth', label: 'dateOfBirth', required: true},
      {key: 'gender', label: 'gender', required: true},
      {key: 'address', label: 'address', required: true},
      {key: 'city', label: 'city', required: true},
      {key: 'zipcode', label: 'zipcode', required: true},
      {key: 'district', label: 'district', required: true},
      {key: 'country', label: 'country', required: false},
      {key: 'vatNumber', label: 'vatNumber', required: false},
      {key: 'fiscalCode', label: 'fiscalCode', required: false},
      {key: 'phoneNumber', label: 'phoneNumber', required: false},
      {key: 'email', label: 'email', required: false},
      {key: 'note', label: 'note', required: false},
      {key: 'externalId', label:'externalId', required: false},
      {key: 'idSalesPoint', label:'idSalesPoint', required: false},
    ],
    perform: createCustomer,
    sample: sample
  }
};
