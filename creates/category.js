const sample = require('../samples/sample_product');

const createCategory = (z, bundle) => {
    return z.request({
        url: 'http://api.cassanova.com/apikey/token',
        headers: {
            'X-Requested-With': '*',
            'Content-Type': 'application/json'
        },
        body: {
          'apiKey': `${bundle.authData.apiKey}`
        },
        params: {},
        method: 'POST'
    })
    .then(response => {
      const answ = JSON.parse(response.content);
      const token = answ.access_token;

      var externalId = "";
      if (`${bundle.inputData.externalId}` == 'undefined') { externalId = null }
      else { externalId = `${bundle.inputData.externalId}` }

      var idSalesPoint = "";
      if (`${bundle.inputData.idSalesPoint}` == 'undefined') { idSalesPoint = null }
      else { idSalesPoint = `${bundle.inputData.idSalesPoint}` }

      var modifiers = "";
      if (`${bundle.inputData.modifiers}` == 'undefined') { modifiers = null }
      else { modifiers = `${bundle.inputData.modifiers}` }

      const par = {
        method: 'POST',
        url: 'https://api.cassanova.com/categories',
        body: {
          'description': `${bundle.inputData.description}`,
          'enableForSale': JSON.parse(`${bundle.inputData.enableForSale}`),
          'enableForRisto': JSON.parse(`${bundle.inputData.enableForRisto}`),
          'enableForECommerce': JSON.parse(`${bundle.inputData.enableForECommerce}`),
          'enableForMobileCommerce': JSON.parse(`${bundle.inputData.enableForMobileCommerce}`),
          'enableForKiosk': JSON.parse(`${bundle.inputData.enableForKiosk}`),
          'enableForSelfOrderMenu': JSON.parse(`${bundle.inputData.enableForSelfOrderMenu}`),
          'externalId': externalId,
          'idSalesPoint': idSalesPoint,
          'modifiers': modifiers
        },
        headers: {
          'X-Version': '1.0.0',
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`,
          'X-Requested-With': '*'
        },
        params: {}
      };
      return z.request(par)
        .then(response2 => {
          return(JSON.parse(response2.content))
        });
    });
};

module.exports = {
  key: 'category',
  noun: 'Category',

  display: {
    label: 'Create Category',
    description: 'Creates a category.'
  },

  operation: {
    inputFields: [
      {key: 'description', label:'description', required: true},
      {key: 'enableForSale', label:'enableForSale', required: true},
      {key: 'enableForRisto', label:'enableForRisto', required: true},
      {key: 'enableForECommerce', label:'enableForECommerce', required: true},
      {key: 'enableForMobileCommerce', label:'enableForMobileCommerce', required: true},
      {key: 'enableForKiosk', label:'enableForKiosk', required: true},
      {key: 'enableForSelfOrderMenu', label:'enableForSelfOrderMenu', required: true},
      {key: 'externalId', label:'externalId', required: false},
      {key: 'idSalesPoint', label:'idSalesPoint', required: false},
      {key: 'modifiers', label:'modifiers', required: false}
    ],
    perform: createCategory,
    sample: sample
  }
};
