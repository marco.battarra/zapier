const sample = require('../samples/sample_product');

const createStockMovement = (z, bundle) => {
    return z.request({
        url: 'http://api.cassanova.com/apikey/token',
        headers: {
            'X-Requested-With': '*',
            'Content-Type': 'application/json'
        },
        body: {
          'apiKey': `${bundle.authData.apiKey}`
        },
        params: {},
        method: 'POST'
    })
    .then(response => {
      const answ = JSON.parse(response.content);
      const token = answ.access_token;

      var idProduct = "";
      if (`${bundle.inputData.idProduct}` == 'undefined') { idProduct = null }
      else { idProduct = `${bundle.inputData.idProduct}` }

      var idProductVariant = "";
      if (`${bundle.inputData.idProductVariant}` == 'undefined') { idProductVariant = null }
      else { idProductVariant = `${bundle.inputData.idProductVariant}` }

      var note = "";
      if (`${bundle.inputData.note}` == 'undefined') { note = null }
      else { note = `${bundle.inputData.note}` }

      const par = {
        method: 'POST',
        url: 'https://api.cassanova.com/stocks/'.concat(`${bundle.inputData.idSalesPoint}`).concat('/movements'),
        body: {
          'idProduct': idProduct,
          'idProductVariant': idProductVariant,
          'quantity': JSON.parse(`${bundle.inputData.quantity}`),
          'note': note,
          'reason': JSON.parse(`${bundle.inputData.reason}`),
        },
        headers: {
          'X-Version': '1.0.0',
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`,
          'X-Requested-With': '*'
        },
        params: {}
      };
      console.log(par);
      return z.request(par)
        .then(response2 => {
          return(JSON.parse(response2.content))
        });
    });
};

module.exports = {
  key: 'stockMovement',
  noun: 'Stock Movement',

  display: {
    label: 'Create Stock Movement',
    description: 'Creates a stock movement.'
  },

  operation: {
    inputFields: [
      {key: 'idProduct', label:'idProduct', required: false},
      {key: 'idProductVariant', label:'idProductVariant', required: false},
      {key: 'reason', label:'reason', required: true},
      {key: 'note', label:'note', required: false},
      {key: 'quantity', label:'quantity', required: true},
      {key: 'idSalesPoint', label:'idSalesPoint', required: true},
    ],
    perform: createStockMovement,
    sample: sample
  }
};
