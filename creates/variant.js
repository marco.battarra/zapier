const sample = require('../samples/sample_product');

const createVariant = (z, bundle) => {
    return z.request({
        url: 'http://api.cassanova.com/apikey/token',
        headers: {
            'X-Requested-With': '*',
            'Content-Type': 'application/json'
        },
        body: {
          'apiKey': `${bundle.authData.apiKey}`
        },
        params: {},
        method: 'POST'
    })
    .then(response => {
      const answ = JSON.parse(response.content);
      const token = answ.access_token;

      var description = "";
      if (`${bundle.inputData.description}` == 'undefined') { description = null }
      else { description = `${bundle.inputData.description}` }

      var prices = "";
      if (`${bundle.inputData.prices}` == 'undefined') { prices = [] }
      else { prices = `${bundle.inputData.prices}` }

      var attributes = "";
      if (`${bundle.inputData.attributes}` == 'undefined') { attributes = [] }
      else { attributes = `${bundle.inputData.attributes }` }

      var costs = "";
      if (`${bundle.inputData.costs}` == 'undefined') { costs = [] }
      else { costs = `${bundle.inputData.costs }` }

      var descriptionOrderTicket = "";
      if (`${bundle.inputData.descriptionOrderTicket}` == 'undefined') { descriptionOrderTicket = [] }
      else { descriptionOrderTicket = `${bundle.inputData.descriptionOrderTicket }` }

      var barcodes = "";
      if (`${bundle.inputData.barcodes}` == 'undefined') { barcodes = [] }
      else { barcodes = `${bundle.inputData.barcodes }` }

      var internalId = "";
      if (`${bundle.inputData.internalId}` == 'undefined') { internalId = [] }
      else { internalId = `${bundle.inputData.internalId }` }

      const par = {
        method: 'POST',
        url: 'https://api.cassanova.com/products/'.concat(`${bundle.inputData.idProduct}`).concat('/variants'),
        body: {
          'description': description,
          'descriptionReceipt': `${bundle.inputData.descriptionReceipt}`,
          'descriptionOrderTicket': descriptionOrderTicket,
          'internalId': internalId,
          'costs': costs,
          'barcodes': barcodes,
          'attributes': attributes,
          'prices': prices,
        },
        headers: {
          'X-Version': '1.0.0',
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`,
          'X-Requested-With': '*'
        },
        params: {}
      };
      return z.request(par)
        .then(response2 => {
          return(JSON.parse(response2.content))
        });
    });
};

module.exports = {
  key: 'variant',
  noun: 'Variant',

  display: {
    label: 'Create Product Variant',
    description: 'Creates a product variant.'
  },

  operation: {
    inputFields: [
      {key: 'idProduct', label: 'idProduct', required: true},
      {key: 'description', label: 'description', required: false},
      {key: 'descriptionReceipt', label: 'descriptionReceipt', required: true},
      {key: 'descriptionOrderTicket', label: 'descriptionOrderTicket', required: false},
      {key: 'internalId', label:'internalId', required: false},
      {key: 'costs', label:'costs', required: false},
      {key: 'barcodes', label:'barcodes', required: false},
      {key: 'attributes', label:'attributes', required: false},
      {key: 'prices', label:'prices', required: false},
    ],
    perform: createVariant,
    sample: sample
  }
};
