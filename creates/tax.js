const sample = require('../samples/sample_product');

const createTax = (z, bundle) => {
    return z.request({
        url: 'http://api.cassanova.com/apikey/token',
        headers: {
            'X-Requested-With': '*',
            'Content-Type': 'application/json'
        },
        body: {
          'apiKey': `${bundle.authData.apiKey}`
        },
        params: {},
        method: 'POST'
    })
    .then(response => {
      const answ = JSON.parse(response.content);
      const token = answ.access_token;

      var externalId = "";
      if (`${bundle.inputData.externalId}` == 'undefined') { externalId = null }
      else { externalId = `${bundle.inputData.externalId}` }

      var idSalesPoint = "";
      if (`${bundle.inputData.idSalesPoint}` == 'undefined') { idSalesPoint = null }
      else { idSalesPoint = `${bundle.inputData.idSalesPoint}` }

      const par = {
        method: 'POST',
        url: 'https://api.cassanova.com/taxes',
        body: {
          'description': `${bundle.inputData.description}`,
          'rate': `${bundle.inputData.rate}`,
          'externalId': externalId,
          'idSalesPoint': idSalesPoint
        },
        headers: {
          'X-Version': '1.0.0',
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`,
          'X-Requested-With': '*'
        },
        params: {}
      };
      return z.request(par)
        .then(response2 => {
          return(JSON.parse(response2.content))
        });
    });
};

module.exports = {
  key: 'tax',
  noun: 'Tax',

  display: {
    label: 'Create Tax',
    description: 'Creates a tax.'
  },

  operation: {
    inputFields: [
      {key: 'description', label:'description', required: true},
      {key: 'rate', label:'rate', required: true},
      {key: 'externalId', label:'externalId', required: false},
      {key: 'idSalesPoint', label:'idSalesPoint', required: false},
    ],
    perform: createTax,
    sample: sample
  }
};
