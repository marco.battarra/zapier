const sample = require('../samples/sample_product');

const createOrganization = (z, bundle) => {
    return z.request({
        url: 'http://api.cassanova.com/apikey/token',
        headers: {
            'X-Requested-With': '*',
            'Content-Type': 'application/json'
        },
        body: {
          'apiKey': `${bundle.authData.apiKey}`
        },
        params: {},
        method: 'POST'
    })
    .then(response => {
      const answ = JSON.parse(response.content);
      const token = answ.access_token;

      var fiscalCode = "";
      if (`${bundle.inputData.fiscalCode}` == 'undefined') { fiscalCode = null }
      else { fiscalCode = `${bundle.inputData.fiscalCode}` }

      var eInvoiceCode = "";
      if (`${bundle.inputData.eInvoiceCode}` == 'undefined') { eInvoiceCode = null }
      else { eInvoiceCode = `${bundle.inputData.eInvoiceCode}` }

      var pec = "";
      if (`${bundle.inputData.pec}` == 'undefined') { pec = null }
      else { pec = `${bundle.inputData.pec}` }

      var phoneNumber = "";
      if (`${bundle.inputData.phoneNumber}` == 'undefined') { phoneNumber = null }
      else { phoneNumber = `${bundle.inputData.phoneNumber}` }

      var email = "";
      if (`${bundle.inputData.email}` == 'undefined') { email = null }
      else { email = `${bundle.inputData.email}` }

      var note = "";
      if (`${bundle.inputData.note}` == 'undefined') { note = null }
      else { note = `${bundle.inputData.note}` }

      var idSalesPoint = "";
      if (`${bundle.inputData.idSalesPoint}` == 'undefined') { idSalesPoint = null }
      else { idSalesPoint = `${bundle.inputData.idSalesPoint}` }

      const par = {
        method: 'POST',
        url: 'https://api.cassanova.com/organizations',
        body: {
          'name': `${bundle.inputData.name}`,
          'address': `${bundle.inputData.address}`,
          'city': `${bundle.inputData.city}`,
          'zipcode': `${bundle.inputData.zipcode}`,
          'district': `${bundle.inputData.district}`,
          'country': `${bundle.inputData.country}`,
          'vatNumber': `${bundle.inputData.vatNumber}`,
          'fiscalCode': fiscalCode,
          'eInvoiceCode': eInvoiceCode,
          'pec': pec,
          'phoneNumber': phoneNumber,
          'email': email,
          'note': note,
          'idSalesPoint': idSalesPoint
        },
        headers: {
          'X-Version': '1.0.0',
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`,
          'X-Requested-With': '*'
        },
        params: {}
      };
      console.log(par);
      return z.request(par)
        .then(response2 => {
          return(JSON.parse(response2.content))
        });
    });
};

module.exports = {
  key: 'organization',
  noun: 'Organization',

  display: {
    label: 'Create Organization',
    description: 'Creates an organization.'
  },

  operation: {
    inputFields: [
      {key: 'name', label: 'name', required: true},
      {key: 'address', label: 'address', required: true},
      {key: 'city', label: 'city', required: true},
      {key: 'zipcode', label: 'zipcode', required: true},
      {key: 'district', label: 'district', required: true},
      {key: 'country', label: 'country', required: true},
      {key: 'vatNumber', label: 'vatNumber', required: true},
      {key: 'fiscalCode', label: 'fiscalCode', required: false},
      {key: 'eInvoiceCode', label: 'eInvoiceCode', required: false},
      {key: 'pec', label: 'pec', required: false},
      {key: 'phoneNumber', label: 'phoneNumber', required: false},
      {key: 'email', label: 'email', required: false},
      {key: 'note', label: 'note', required: false},
      {key: 'idSalesPoint', label:'idSalesPoint', required: false},
    ],
    perform: createOrganization,
    sample: sample
  }
};
