const productCreate = require('./creates/product');
const taxCreate = require('./creates/tax');
const departmentCreate = require('./creates/department');
const optionCreate = require('./creates/option');
const categoryCreate = require('./creates/category');
const customerCreate = require('./creates/customer');
const variantCreate = require('./creates/variant');
const supplierCreate = require('./creates/supplier');
const stockMovementCreate = require('./creates/stockMovement');
const organizationCreate = require('./creates/organization');
const salesModeCreate = require('./creates/salesMode');
//const productTrigger = require('./triggers/product');
const authentication = require('./authentication');

const addAuthHeader = (z, bundle) => {
    return z.request({
        url: 'http://api.cassanova.com/apikey/token',
        headers: {
            'X-Requested-With': '*',
            'Content-Type': 'application/json'
        },
        body: {
          'apiKey': `${bundle.authData.apiKey}`
        },
        params: {},
        method: 'POST'
    })
    .then(response => {
      if (response.status === 401) {
        throw new Error('The API Key you supplied is invalid');
      }
      const answ = JSON.parse(response.content);
      const token = answ.access_token;
      request.headers.Authorization = token;
      return request;
    });
};

const handleHTTPError = (response, z) => {
  if (response.status >= 400) {
    throw new Error(`Unexpected status code ${response.status}`);
  }
  return response;
};

const App = {

  // Reference to the installed dependencies
  version: require('./package.json').version,
  platformVersion: require('zapier-platform-core').version,

  authentication: authentication.authentication,

  // Include API key request on all outbound requests
  beforeRequest: [
//    addAuthHeader
  ],

  afterResponse: [
    handleHTTPError
  ],

  resources: {
  },

  triggers: {
//    [productTrigger.key]: productTrigger,
  },

  searches: {
  },

  creates: {
    [productCreate.key]: productCreate,
    [taxCreate.key]: taxCreate,
    [departmentCreate.key]: departmentCreate,
    [optionCreate.key]: optionCreate,
    [categoryCreate.key]: categoryCreate,
    [customerCreate.key]: customerCreate,
    [variantCreate.key]: variantCreate,
    [supplierCreate.key]: supplierCreate,
    [stockMovementCreate.key]: stockMovementCreate,
    [organizationCreate.key]: organizationCreate,
    [salesModeCreate.key]: salesModeCreate,
  }

};

// Finally, export the app.
module.exports = App;
