require('should');

const zapier = require('zapier-platform-core');

const App = require('../index');
const salesMode = zapier.createAppTester(App);

describe('App.creates.salesMode', () => {

  it('passes authentication and returns json', (done) => {

    const bundle = {
      authData: {
        apiKey: '3386e49b-6f10-40e0-9b34-36dc0a2bca37'
      },
      inputData: {
        description: 'Item test API zapier',
        idSalesPoint: 94
      },
    };

    /*product(App.triggers.product.operation.perform, bundle)
      .then((response) => {
        console.log(response);
        done();
      })
      .catch(done);
    });*/

    salesMode(App.creates.salesMode.operation.perform, bundle)
      .then((response) => {
        console.log(response);
        done();
      })
      .catch(done);
    });

});
