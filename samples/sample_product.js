module.exports = {
            "id": "e7df1ceb-6512-477f-98dd-e6b734a4bcde",
            "description": "Ammammeta",
            "descriptionLabel": "Ammammeta",
            "idDepartment": "7bc7dd22-6dc9-4b9c-bff7-580cf399205a",
            "department": {
                "id": "7bc7dd22-6dc9-4b9c-bff7-580cf399205a",
                "description": "Rep2",
                "descriptionLabel": "Rep2",
                "descriptionReceipt": "Rep2",
                "idTax": "02a72734-497c-4174-969b-1618b9452f46",
                "tax": {
                    "id": "02a72734-497c-4174-969b-1618b9452f46",
                    "description": "Iva 22%",
                    "rate": 22,
                    "externalId": "E",
                    "lastUpdate": 1535448201521
                },
                "color": "98c32c",
                "lastUpdate": 1459860838139
            },
            "idCategory": "fcd08acf-0eb4-40d1-ad80-33b997886a1f",
            "category": {
                "id": "fcd08acf-0eb4-40d1-ad80-33b997886a1f",
                "description": "CategoryListinoTest 1",
                "externalId": "10",
                "enableForRisto": true,
                "enableForSale": true,
                "enableForECommerce": true,
                "enableForMobileCommerce": true,
                "enableForSelfOrderMenu": true,
                "enableForKiosk": true,
                "lastUpdate": 1544706827650
            },
            "soldByWeight": false,
            "multivariant": false,
            "enableForRisto": true,
            "enableForSale": true,
            "enableForECommerce": true,
            "enableForMobileCommerce": true,
            "enableForSelfOrderMenu": true,
            "enableForKiosk": true,
            "descriptionReceipt": "Ammammeta",
            "internalId": "12345",
            "barcodes": [
                {
                    "value": "ABC123",
                    "salable": true
                },
                {
                    "value": "abc123",
                    "salable": true
                }
            ],
            "variants": [
                {
                    "id": "f598b093-ffa5-444a-9c57-afe93eb41f02",
                    "description": "Ammammeta",
                    "descriptionReceipt": "Ammammeta",
                    "internalId": "12345",
                    "barcodes": [
                        {
                            "value": "ABC123",
                            "salable": true
                        },
                        {
                            "value": "abc123",
                            "salable": true
                        }
                    ]
                }
            ],
            "prices": [
                {
                    "idSalesPoint": 94,
                    "value": 10
                }
            ],
            "externalId": "12345",
            "idSalesPoint": 94,
            "lastUpdate": 1548930190036
}
