const testAuth = (z, bundle) => {
    return z.request({
        url: 'http://api.cassanova.com/apikey/token',
        headers: {
            'X-Requested-With': '*',
            'Content-Type': 'application/json'
        },
        body: {
          'apiKey': `${bundle.authData.apiKey}`
        },
        params: {},
        method: 'POST'
    })
    .then(response => {
      if (response.status === 401) {
        throw new Error('The API Key you supplied is invalid');
      }
      const answ = JSON.parse(response.content);
      const token = answ.access_token; 
      return token;
    });
};

/**
 * The custom authentication object
 */
const authentication = {
    type: 'custom',//'session',
    // If you need any fields upfront
    test: testAuth,
    fields: [
      {key: 'apiKey', label: 'API Key', required: true, type: 'string'}
    ],
    connectionLabel: (z, bundle) => {
      return bundle.authData.apiKey;
    //connectionLabel: '{{bundle.authData.access_token}}'
    }
};

module.exports = {
    authentication
};
